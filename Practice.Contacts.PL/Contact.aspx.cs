﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Practice.Contacts.PL
{
    public partial class Contact : System.Web.UI.Page
    {
        /// <summary>
        /// transversal object for business layer
        /// </summary>
        private static BL.Contact ContactBL = new BL.Contact();

        /// <summary>
        /// Load the page
        /// </summary>
        /// <param name="sender-"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    this.InitUI();
                }
                catch (Exception ex)
                {
                    this.ShowError(ex.Message);
                }
            }
        }

        /// <summary>
        /// Search for employees
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string page = GetCurrentUrl();
            string firstName = this.txtFirstName.Text.Trim();
            string lastname = this.txtLastName.Text.Trim();
            string phone = this.txtNumberPhone.Text.Trim();
            string mail = this.txtMail.Text.Trim();
            string url = string.Format("{0}?action=search&firstname={1}&lastname={2}&numberphone={3}&mail={4}",
                page,
                firstName,
                lastname,
                phone,
                mail);
            this.Redirect(url);
                
        }

        /// <summary>
        /// Clear search filters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearchClear_Click(object sender, EventArgs e)
        {
            this.Redirect(this.GetCurrentUrl());
        }

        /// <summary>
        /// Initializing the interface
        /// </summary>
        private void InitUI()
        {
            List<EL.Contact> contacts = this.GetContacts();
            this.PopulateContacts(contacts);
            this.PopulateSearchFilters();
        }

        /// <summary>
        /// Gets Contacts
        /// </summary>
        /// <returns></returns>
        private List<EL.Contact> GetContacts()
        {
            List<EL.Contact> rs = new List<EL.Contact>();
            if (IsSearch())
            {
                rs = this.SearchContacts(this.GetFilters());
            }
            else
            {
                rs = this.GetAllContacts();
            }
            return rs;
        }

        /// <summary>
        /// Search for employees
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        private List<EL.Contact> SearchContacts(EL.Contact filters)
        {
            return ContactBL.SearchContact(filters);
        }

        /// <summary>
        /// Get all employees.
        /// </summary>
        /// <returns></returns>
        private List<EL.Contact> GetAllContacts()
        {
            return ContactBL.GetAll();
        }

        /// <summary>
        /// Fill the employee grid.
        /// </summary>
        /// <param name="data"></param>
        private void PopulateContacts(List<EL.Contact> data)
        {
            this.gvContacts.DataSource = data;
            this.gvContacts.DataBind();
        }

        /// <summary>
        /// Gets employee search filters
        /// </summary>
        /// <returns></returns>
        private EL.Contact GetFilters()
        {
            EL.Contact rs = new EL.Contact();
            rs.FirstName = this.GetQueryStringParameter("firstname");
            rs.LastName = this.GetQueryStringParameter("lastname");
            rs.NumberPhone = this.GetQueryStringParameter("numberphone");
            rs.Mail = this.GetQueryStringParameter("mail");
            
            return rs;
        }

        /// <summary>
        /// Fill in the fields of the search form
        /// </summary>
        private void PopulateSearchFilters()
        {
            this.txtFirstName.Text = string.Empty;
            this.txtLastName.Text = string.Empty;
            this.txtMail.Text = string.Empty;
            this.txtNumberPhone.Text = string.Empty;
            if (IsSearch())
            {
                this.txtFirstName.Text = this.GetQueryStringParameter("firstname");
                this.txtLastName.Text = this.GetQueryStringParameter("lastname");
                this.txtMail.Text = this.GetQueryStringParameter("mail");
                this.txtNumberPhone.Text = this.GetQueryStringParameter("numberphone");
            }
        }


        /// <summary>
        /// Gets the value of a parameter of the querystring
        /// </summary>
        /// <param name="parameterName"></param>
        /// <returns></returns>
        private string GetQueryStringParameter(string parameterName)
        {
            string paramValue = null;
            if (Request.QueryString[parameterName] != null)
            {
                paramValue = Request.QueryString[parameterName].ToString().Trim();
            }
            return paramValue;
        }

        /// <summary>
        /// Indicates whether the page is loading in search mode
        /// </summary>
        /// <returns></returns>
        private bool IsSearch()
        {
            return (!string.IsNullOrEmpty(this.GetQueryStringParameter("action")));
        }


        /// <summary>
        /// Gets the current url
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private string GetCurrentUrl(string filePath = "")
        {
            var url = string.Empty;
            var context = HttpContext.Current;
            if (context != null)
            {
                filePath = (string.IsNullOrEmpty(filePath) ? context.Request.FilePath : filePath).Trim().TrimStart('/');
                url = string.Format("{0}://{1}{2}{3}{4}",
                    context.Request.Url.Scheme,
                    context.Request.Url.Host,
                    context.Request.Url.Port == 80 ? string.Empty : ":" + context.Request.Url.Port,
                    context.Request.ApplicationPath.Trim('/') + "/",
                    filePath);
            }
            return url;
        }

        /// <summary>
        /// Displays an error message on the screen
        /// </summary>
        /// <param name="error"></param>
        private void ShowError(string error)
        {
            this.pnlErrorMessage.Visible = true;
            this.lblErrorMessage.Text = error;
        }

        /// <summary>
        /// Redirects to a specific url
        /// </summary>
        /// <param name="url"></param>
        private void Redirect(string url)
        {
            Response.Redirect(url);
        }
    }
}