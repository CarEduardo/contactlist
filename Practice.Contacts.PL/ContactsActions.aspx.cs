﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Practice.Contacts.PL
{
    public partial class ContactsActions : System.Web.UI.Page
    {
       
        /// <summary>
        /// transversal object for business layer
        /// </summary>
        private static BL.Contact ContactBL = new BL.Contact();

        /// <summary>
        /// Types of action
        /// </summary>
        private enum ActionType
        {
            None,
            Add,
            Edit,
            Delete
        };

        /// <summary>
        /// Stores the type of action to execute
        /// </summary>
        private static ActionType Action;
    
        /// <summary>
        /// Event that runs when loading page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    this.SetAction();
                    this.RenderAction(Action);
                }
                catch (Exception ex)
                {
                    this.ShowError(ex.Message);
                }
            }
        }

        /// <summary>
        /// Save contact
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                EL.Contact contact = this.GetFormData();
                if (Action == ActionType.Edit)
                {
                    contact.ContactId = this.GetContactId();
                }
                EL.Contact savedContact = ContactBL.Upsert(contact);
                if (savedContact.IsValid == false)
                {
                    this.ShowError(savedContact.Message);
                }
                else
                {
                    Redirect();
                }
            }
            catch (Exception ex)
            {
                this.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// Cancel the save
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            this.Redirect();
        }

        /// <summary>
        /// Delete an contact
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDeleteOk_Click(object sender, EventArgs e)
        {
            try
            {
                EL.Contact contact = new EL.Contact()
                {
                    ContactId = this.GetContactId()
                };
                var deletedContact = ContactBL.Delete(contact);
                Redirect();
            }
            catch (Exception ex)
            {
                this.ShowError(ex.Message);
            }
        }

        /// <summary>
        /// Cancel the removal of an contact
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDeleteCancel_Click(object sender, EventArgs e)
        {
            this.Redirect();
        }

        /// <summary>
        /// Displays the corresponding action panel
        /// </summary>
        /// <param name="action"></param>
        private void RenderAction(ActionType action)
        {
            try
            {
                this.pnlUpsert.Visible = false;
                this.pnlDelete.Visible = false;
                EL.Contact contact = new EL.Contact() 
                {
                    ContactId = this.GetContactId()
                };
                switch (action)
                {
                    case ActionType.Add:
                        this.lblActionName.Text = "Add Contact";
                        this.pnlUpsert.Visible = true;
                        break;
                    case ActionType.Edit:
                        this.lblActionName.Text = "Edit Contact";
                        if (contact.ContactId <= 0)
                        {
                            throw new Exception("Contact id not specified.");
                        }
                        this.Populate(ContactBL.GetById(contact));
                        this.pnlUpsert.Visible = true;
                        break;
                    case ActionType.Delete:
                        this.lblActionName.Text = "Delete Contact";
                        if (contact.ContactId <= 0)
                        {
                            throw new Exception("Contact id not specified.");
                        }
                        this.pnlDelete.Visible = true;
                        break;                    
                    default:
                        this.lblActionName.Text = "An error occurred while trying to render the action!";
                        throw new Exception("Invalid action.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// populate the form fields
        /// </summary>
        /// <param name="contact"></param>
        private void Populate(EL.Contact contact)
        {
            this.hdnContactId.Value = contact.ContactId.ToString();
            this.txtFirstName.Text = contact.FirstName;
            this.txtLastName.Text = contact.LastName;
            this.txtNumberPhone.Text = contact.NumberPhone;
            this.txtMail.Text = contact.Mail;
        }

        /// <summary>
        /// Gets contact id from the query string parameters 
        /// </summary>
        /// <returns></returns>
        private int GetContactId()
        {
            int contactId = 0;
            if (Request.QueryString["contactId"] != null)
            {
                int.TryParse(Request.QueryString["contactId"].ToString().Trim(), out contactId);
            }
            return contactId;
        }

        /// <summary>
        /// Gets the action name from the query string parameters
        /// </summary>
        /// <returns></returns>
        private string GetActionName()
        {
            string actionName = string.Empty;
            if (Request.QueryString["action"] != null)
            {
                actionName = Request.QueryString["action"].ToString().Trim().ToLower();
            }
            return actionName;
        }

        /// <summary>
        /// Sets the action to execute
        /// </summary>
        private void SetAction()
        {
            Action = ActionType.None;
            string actionName = this.GetActionName();
            if (actionName != string.Empty)
            {
                actionName = Request.QueryString["action"].ToString().Trim().ToLower();
                switch (actionName)
                {
                    case "add":
                        Action = ActionType.Add;
                        break;
                    case "edit":
                        Action = ActionType.Edit;
                        break;
                    case "delete":
                        Action = ActionType.Delete;
                        break;
                }
            }
        }

        /// <summary>
        /// Gets form data
        /// </summary>
        /// <returns></returns>
        private EL.Contact GetFormData()
        {
            EL.Contact rs = new EL.Contact();
            try
            {
                rs.ContactId = int.Parse(this.hdnContactId.Value.ToString());
                rs.FirstName = this.txtFirstName.Text;
                rs.LastName = this.txtLastName.Text;
                rs.NumberPhone = this.txtNumberPhone.Text;
                rs.Mail = this.txtMail.Text;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while fetching form data: " + ex.Message);
            }
            return rs;
        }

        /// <summary>
        /// Gets the current url
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private string GetCurrentUrl(string filePath = "")
        {
            var url = string.Empty;
            var context = HttpContext.Current;
            if (context != null)
            {
                filePath = (string.IsNullOrEmpty(filePath) ? context.Request.FilePath : filePath).Trim().TrimStart('/');
                url = string.Format("{0}://{1}{2}{3}{4}",
                    context.Request.Url.Scheme,
                    context.Request.Url.Host,
                    context.Request.Url.Port == 80 ? string.Empty : ":" + context.Request.Url.Port,
                    context.Request.ApplicationPath.Trim('/') + "/",
                    filePath);
            }
            return url;
        }

        /// <summary>
        /// Redirects to a specific url
        /// </summary>
        private void Redirect()
        {
            Response.Redirect(this.GetCurrentUrl("Contact.aspx"));
        }

        /// <summary>
        /// Displays an error message on the screen
        /// </summary>
        /// <param name="error"></param>
        private void ShowError(string error)
        {
            this.pnlErrorMessage.Visible = true;
            this.lblErrorMessage.Text = error;
        }

    }
}