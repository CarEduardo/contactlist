﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="Practice.Contacts.PL.Contact" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12 text-right">
            <asp:Button runat="server" ID="btnAdd" CssClass="btn btn-primary" Text="Add New Contact" PostBackUrl="~/ContactsActions.aspx?action=add" />
            <button class="btn btn-danger" type="button" data-toggle="collapse" data-target="#searchFilters" aria-expanded="false" aria-controls="searchFilters">
                  Contact Search
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <hr />
        </div>
    </div>
    <div id="searchFilters" class="collapse">
        <div class="row">
            <div class="col-lg-4">
                <asp:Panel runat="server" ID="pnlErrorMessage" Visible="false">
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <asp:Label runat="server" ID="lblErrorMessage" Text=""></asp:Label>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="txtFirstName">First Name</label>
                    <asp:TextBox runat="server" ID="txtFirstName" CssClass="form-control" placeholder="First Name"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="txtLastName">Last Name</label>
                    <asp:TextBox runat="server" ID="txtLastName" CssClass="form-control" placeholder="Last Name"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="txtLastName">Number Phone</label>
                    <asp:TextBox runat="server" ID="txtNumberPhone" CssClass="form-control" placeholder="Number Phone"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="txtLastName">E-mail</label>
                    <asp:TextBox runat="server" ID="txtMail" CssClass="form-control" placeholder="E-mail"></asp:TextBox>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="btnSave">&nbsp;</label><br />
                    <asp:Button runat="server" ID="btnSearch" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click" />
                    <asp:Button runat="server" ID="btnSearchClear" CssClass="btn btn-danger" Text="Clear Filters" OnClick="btnSearchClear_Click"/>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <hr />
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <asp:GridView ID="gvContacts" runat="server" AutoGenerateColumns="False" CssClass="table table-condensed table-hover table-bordered table-striped">
                <Columns>
                    <asp:BoundField DataField="ContactId" HeaderText="Id Contact" Visible="false"/>
                    <asp:BoundField DataField="FirstName" HeaderText="First Name"/>
                    <asp:BoundField DataField="LastName" HeaderText="Last Name"/>
                    <asp:BoundField DataField="NumberPhone" HeaderText="Phone"/>
                    <asp:BoundField DataField="Mail" HeaderText="E-mail" />
                    
                    <asp:HyperLinkField DataNavigateUrlFields="ContactId" DataNavigateUrlFormatString="ContactsActions.aspx?action=edit&contactId={0}" Text="Edit" HeaderText="Edit" />
                    <asp:HyperLinkField DataNavigateUrlFields="ContactId" DataNavigateUrlFormatString="ContactsActions.aspx?action=delete&contactId={0}" Text="Delete" HeaderText="Delete" />
                </Columns>
                <EmptyDataTemplate>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <p class="text-danger">No contacts found!</p>
                    </div>
                </EmptyDataTemplate>
            </asp:GridView>
        </div>
    </div>
</asp:Content>