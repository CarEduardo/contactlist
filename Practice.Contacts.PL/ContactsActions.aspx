﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="ContactsActions.aspx.cs" Inherits="Practice.Contacts.PL.ContactsActions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1><asp:Label runat="server" Text="" ID="lblActionName"></asp:Label></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <hr />
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

            <asp:Panel runat="server" ID="pnlErrorMessage" Visible="false">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <asp:Label runat="server" ID="lblErrorMessage" Text=""></asp:Label>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlUpsert" Visible="false">
                <asp:HiddenField ID="hdnContactId" runat="server" value="0" />                
                <div class="form-group">
                    <label for="txtFirstName">First Name</label>
                    <asp:TextBox runat="server" ID="txtFirstName" CssClass="form-control" placeholder="First Name"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="txtLastName">Last Name</label>
                    <asp:TextBox runat="server" ID="txtLastName" CssClass="form-control" placeholder="Last Name"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="txtNumberPhone">Number Phone</label>
                    <asp:TextBox runat="server" ID="txtNumberPhone" CssClass="form-control" placeholder="Number Phone"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label for="txtMail">E-mail</label>
                    <asp:TextBox runat="server" ID="txtMail" CssClass="form-control" placeholder="E-mail"></asp:TextBox>
                </div>
                <asp:Button runat="server" ID="btnSave" CssClass="btn btn-primary" Text="Save" OnClick="btnSave_Click" />
                <asp:Button runat="server" ID="btnCancel" CssClass="btn btn-danger" Text="Cancel" OnClick="btnCancel_Click" />
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlDelete" Visible="false">
                <div class="panel panel-warning">
                    <div class="panel-heading">Confirm Dialog</div>
                    <div class="panel-body">
                        <p class="text-danger">¿Are you sure?</p>
                        <asp:Button ID="btnDeleteOk" runat="server" CssClass="btn btn-primary" Text="Ok" OnClick="btnDeleteOk_Click" />
                        <asp:Button ID="btnDeleteCancel" runat="server" CssClass="btn btn-danger" Text="Cancel" OnClick="btnDeleteCancel_Click" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>

