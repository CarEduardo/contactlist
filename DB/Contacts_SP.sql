
USE Contacts
GO
-- #######################################################
-- ## CEH, 31/06/2017, Inserta o actualiza un contacto  ##
-- #######################################################
IF NOT EXISTS (SELECT OBJECT_ID FROM SYS.OBJECTS WHERE TYPE = 'P' AND OBJECT_ID = OBJECT_ID('dbo.Contacts_Upsert'))
BEGIN
   EXEC('CREATE PROCEDURE [dbo].[Contacts_Upsert] AS BEGIN SET NOCOUNT ON; END')
END

GO

ALTER PROCEDURE [dbo].[Contacts_Upsert] 
	@ContactId	INT
	,@FirstName	VARCHAR(50)
	,@LastName	VARCHAR(100)
	,@NumberPhone VARCHAR(15)
	,@Mail VARCHAR(50)
AS
BEGIN

	IF (@ContactId IS NULL OR @ContactId = 0)
	BEGIN
		
		INSERT INTO Contacts 
		(			
			FirstName
			,LastName
			,NumberPhone
			,Mail
		)
		VALUES 
		(	
			@FirstName
			,@LastName
			,@NumberPhone
			,@Mail
		);

		-- Retornar el ultimo registro insertado
		SELECT CAST(SCOPE_IDENTITY() AS INT) AS EmployeeId;
	END
	ELSE
	BEGIN

		UPDATE Contacts 
		SET FirstName = @FirstName
			,LastName = @LastName
			,NumberPhone = @NumberPhone
			,Mail = @Mail
		WHERE ContactId = @ContactId;

		-- Retornar el registro actualizado
		SELECT @ContactId AS ContactId;
	END
END

GO


-- ##################################################
-- ## CEH, 31/06/2017, Obtiene todos los contactos ##
-- ##################################################
IF NOT EXISTS (SELECT OBJECT_ID FROM SYS.OBJECTS WHERE TYPE = 'P' AND OBJECT_ID = OBJECT_ID('dbo.Contact_GetAll'))
BEGIN
   EXEC('CREATE PROCEDURE [dbo].[Contact_GetAll] AS BEGIN SET NOCOUNT ON; END')
END

GO

ALTER PROCEDURE [dbo].[Contact_GetAll] 
AS
BEGIN
	-- Obtener todos los registros de la tabla
	SELECT ContactId
		,FirstName
		,LastName
		,NumberPhone
		,Mail
	FROM Contacts 
END

GO
-- ######################################################
-- ## CEH, 31/06/2017, Obtiene a un contacto por su id ##
-- ######################################################
IF NOT EXISTS (SELECT OBJECT_ID FROM SYS.OBJECTS WHERE TYPE = 'P' AND OBJECT_ID = OBJECT_ID('dbo.Contact_GetById'))
BEGIN
   EXEC('CREATE PROCEDURE [dbo].[Contact_GetById] AS BEGIN SET NOCOUNT ON; END')
END

GO

ALTER PROCEDURE [dbo].[Contact_GetById] 
	@ContactId INT
AS
BEGIN
	-- Obtiene los datos de un empleado
	SELECT TOP 1 ContactId 
			,FirstName
			,LastName
			,NumberPhone
			,Mail
	FROM Contacts 
	WHERE ContactId =  @ContactId
END

GO

-- ##################################################
-- ## CEH, 31/06/2017, Elimina un contacto por id  ##
-- ##################################################
IF NOT EXISTS (SELECT OBJECT_ID FROM SYS.OBJECTS WHERE TYPE = 'P' AND OBJECT_ID = OBJECT_ID('dbo.Contact_Delete'))
BEGIN
   EXEC('CREATE PROCEDURE [dbo].[Contact_Delete] AS BEGIN SET NOCOUNT ON; END')
END

GO

ALTER PROCEDURE [dbo].[Contact_Delete] 
	@ContactId	INT
AS
BEGIN

	IF (@ContactId IS NOT NULL)
	BEGIN

		DELETE 
		FROM Contacts 
		WHERE ContactId = @ContactId

	END

	-- Retornar el registro eliminado
	SELECT @ContactId
END

GO
-- ###############################################################################################
-- ## CEH, 31/06/2017, Realiza una busqueda de contactos que cumplan con los parametros pasados ##
-- ###############################################################################################
IF NOT EXISTS (SELECT OBJECT_ID FROM SYS.OBJECTS WHERE TYPE = 'P' AND OBJECT_ID = OBJECT_ID('dbo.Contact_Search'))
BEGIN
   EXEC('CREATE PROCEDURE [dbo].[Contact_Search] AS BEGIN SET NOCOUNT ON; END')
END

GO

ALTER PROCEDURE [dbo].[Contact_Search] 
	@FirstName	VARCHAR(50)=''
	,@LastName	VARCHAR(100)=''
	,@NumberPhone VARCHAR(15)=''
	,@Mail VARCHAR(50)=''
AS
BEGIN
	-- Obtiene los empleados encontrados
	SELECT ContactId
		,FirstName
		,LastName
		,NumberPhone
		,Mail
	FROM Contacts 
	WHERE ((@FirstName = '' OR FirstName LIKE '%' + @FirstName + '%' COLLATE LATIN1_GENERAL_CI_AI) AND
		(@LastName = '' OR LastName LIKE '%' + @LastName + '%' COLLATE LATIN1_GENERAL_CI_AI) AND
		(@NumberPhone = '' OR NumberPhone LIKE '%' + @NumberPhone + '%' COLLATE LATIN1_GENERAL_CI_AI) AND
		(@Mail ='' OR Mail LIKE '%' + @Mail + '%' COLLATE LATIN1_GENERAL_CI_AI))
END

GO

-- ###############################################################################################
-- ## CEH, 31/06/2017, Realiza una busqueda para la validacion de correo y telefonos duplicados ##
-- ###############################################################################################
IF NOT EXISTS (SELECT OBJECT_ID FROM SYS.OBJECTS WHERE TYPE = 'P' AND OBJECT_ID = OBJECT_ID('dbo.Contacts_Search_PhoneEmail'))
BEGIN
   EXEC('CREATE PROCEDURE [dbo].[Contacts_Search_PhoneEmail] AS BEGIN SET NOCOUNT ON; END')
END

GO

ALTER PROCEDURE [dbo].[Contacts_Search_PhoneEmail] 
	 @NumberPhone VARCHAR(15)
	,@Mail VARCHAR(50)
AS
BEGIN
	SELECT TOP 1 ContactId 
			,FirstName
			,LastName
			,NumberPhone
			,Mail
	FROM Contacts 
	WHERE Mail = @Mail OR NumberPhone = @NumberPhone
END