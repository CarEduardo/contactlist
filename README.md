Agenda Asp.Net
===================


Este pequeño proyecto está desarrollado con ASP.NET con la arquitectura N capas, donde se muestra una pequeña agenda con el cual podremos administrar contactos, esta agenda cuenta con algunas validaciones como por ejemplo de correo y teléfono. 

----------

### Requerimientos

- Visual Studio 2012

- Sql Server 2016


Configuración 
-------------

Para poner en marcha el proyecto es necesario hacer algunas configuraciones tanto en la aplicación como en la creación de la base de datos.

#### **Base de datos**

En la carpeta **DB** se encuentran dos script los cuales sirven para la construcción de la base de datos así como los procedimientos almacenados.

La manera en que deben de ejecutarse los scripts es la siguiente.

> - El script de nombre **Contacts_db.sql** es el primero que debe ser ejecutado ya que nos ayuda a la creación de la base de datos.
> - Lo siguiente es ejecutar los procedimientos almacenados de los cuales hace uso la aplicación, el script lo podemos hallar con el nombre de **contactos_sp.sql** 

#### **Aplicación**
Dentro de la solución tendremos que configurar la conexión a la base de datos, el nombre del archivo a modificar es **Web.config** el cual lo podemos encontrar el la ruta siguiente:

```
contactList
└── Practice.Contacts.PL
    └── Web.config
```

A continuación encontraremos la siguiente configuración, nos dirigimos en la parte **connectionString** y la cambiamos por nuestra conexión.
```
<configuration>
    <system.web>
      <compilation debug="true" targetFramework="4.5" />
      <httpRuntime targetFramework="4.5" />
    </system.web>
    <connectionStrings>
      <add name="Main" connectionString="data source=LAPANITA\SQLEXPRESS;initial catalog=Contacts;persist security info=True;Integrated Security = true;"/>
    </connectionStrings>
</configuration>
```
podemos seguir el siguiente [link](https://www.connectionstrings.com/sql-server/) para ayudarnos a usar un **connectionString**

Una vez configurado nuestro proyecto podemos inicia probarlo con la siguinte ruta

> **Nota**
> Solo cambiar la direccion del IIS y el puerto, lo siguiente es un ejemplo 
> 127.0.0.1:8080/Contact.aspx