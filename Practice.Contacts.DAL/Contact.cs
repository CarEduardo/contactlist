﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Practice.Contacts.EL;

namespace Practice.Contacts.DAL
{
    public class Contact
    {
        private ConnectionString connectionString;

        public Contact()
        {
            this.connectionString = new ConnectionString();
        }

        public EL.Contact GetContact(EL.Contact contact) {
            return new EL.Contact { FirstName="Carlos", LastName="Hernandez", Mail="carlosedhndez@gmail.com", NumberPhone="7711509392" };
        }

        public EL.Contact GetById(EL.Contact contact)
        {
            EL.Contact rs = new EL.Contact();

            try
            {
                using (SqlConnection con = new SqlConnection(this.connectionString.ToString()))
                {
                    con.Open();
                    using (SqlCommand com = new SqlCommand())
                    {
                        com.CommandText = "Contact_GetById";
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.Parameters.Add(new SqlParameter("ContactId", contact.ContactId));
                        com.Connection = con;
                        com.CommandTimeout = 90;
                        SqlDataReader reader = com.ExecuteReader(System.Data.CommandBehavior.SingleResult);
                        while (reader.Read())
                        {
                            rs = this.EntityDataBind(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while trying to get the contact data from the database: " + ex.Message);
            }

            return rs;
        }

        public EL.Contact SearchPhoneEmail(EL.Contact contact)
        {
            EL.Contact rs = new EL.Contact();

            try
            {
                using (SqlConnection con = new SqlConnection(this.connectionString.ToString()))
                {
                    con.Open();
                    using (SqlCommand com = new SqlCommand())
                    {
                        com.CommandText = "Contacts_Search_PhoneEmail";
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.Parameters.Add(new SqlParameter("NumberPhone", contact.NumberPhone));
                        com.Parameters.Add(new SqlParameter("Mail", contact.Mail));
                        com.Connection = con;
                        com.CommandTimeout = 90;
                        SqlDataReader reader = com.ExecuteReader(System.Data.CommandBehavior.SingleResult);
                        while (reader.Read())
                        {
                            rs = this.EntityDataBind(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while trying to get the contact data from the database: " + ex.Message);
            }

            return rs;
        }

        public List<EL.Contact> GetAll()
        {
            List<EL.Contact> rs = new List<EL.Contact>();

            try
            {
                using (SqlConnection con = new SqlConnection(this.connectionString.ToString()))
                {
                    con.Open();
                    using (SqlCommand com = new SqlCommand())
                    {
                        com.CommandText = "Contact_GetAll";
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.Connection = con;
                        com.CommandTimeout = 90;
                        SqlDataReader reader = com.ExecuteReader();
                        while (reader.Read())
                        {
                            rs.Add(this.EntityDataBind(reader));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while fetching contacts from the database: " + ex.Message);
            }

            return rs; 
        }

        public EL.Contact UpsertContact(EL.Contact contact)
        {
            EL.Contact rs = new EL.Contact();
            try
            {
                using (SqlConnection con = new SqlConnection(this.connectionString.ToString()))
                {
                    con.Open();
                    using (SqlCommand com = new SqlCommand())
                    {
                        com.CommandText = "Contacts_Upsert";
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.Connection = con;
                        com.CommandTimeout = 90;
                        com.Parameters.Add(new SqlParameter("ContactId", contact.ContactId));
                        com.Parameters.Add(new SqlParameter("FirstName", contact.FirstName));
                        com.Parameters.Add(new SqlParameter("LastName", contact.LastName));
                        com.Parameters.Add(new SqlParameter("NumberPhone", contact.NumberPhone));
                        com.Parameters.Add(new SqlParameter("Mail", contact.Mail));
                        int value = (int)com.ExecuteScalar();
                        rs.ContactId = value;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while trying to save the contact data to the database: " + ex.Message);
            }

            return rs;        
        }

        public List<EL.Contact> SearchContact(EL.Contact contact)
        {
            List<EL.Contact> rs = new List<EL.Contact>();
            try
            {
                using (SqlConnection con = new SqlConnection(this.connectionString.ToString()))
                {
                    con.Open();
                    using (SqlCommand com = new SqlCommand())
                    {
                        com.CommandText = "Contact_Search";
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.Connection = con;
                        com.CommandTimeout = 90;
                        com.Parameters.Add(new SqlParameter("FirstName", contact.FirstName));
                        com.Parameters.Add(new SqlParameter("LastName", contact.LastName));
                        com.Parameters.Add(new SqlParameter("NumberPhone", contact.NumberPhone));
                        com.Parameters.Add(new SqlParameter("Mail", contact.Mail));
                        SqlDataReader reader = com.ExecuteReader();
                        while (reader.Read())
                        {
                            rs.Add(this.EntityDataBind(reader));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while fetching contacts from the database: " + ex.Message);
            }

            return rs;   
        }

        public EL.Contact DeleteContact(EL.Contact contact)
        {
            EL.Contact rs = new EL.Contact();
            try
            {
                using (SqlConnection con = new SqlConnection(this.connectionString.ToString()))
                {
                    con.Open();
                    using (SqlCommand com = new SqlCommand())
                    {
                        com.CommandText = "Contact_Delete";
                        com.CommandType = System.Data.CommandType.StoredProcedure;
                        com.Connection = con;
                        com.CommandTimeout = 90;
                        com.Parameters.Add(new SqlParameter("ContactId", contact.ContactId));
                        int value = (int)com.ExecuteScalar();
                        rs.ContactId = value;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while trying to remove the contacts from the database: " + ex.Message);
            }

            return rs;
        }

        private EL.Contact EntityDataBind(SqlDataReader reader)
        {
            EL.Contact rs = new EL.Contact();
            try
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ContactId")))
                {
                    rs.ContactId = reader.GetInt32(reader.GetOrdinal("ContactId"));
                }
                if (!reader.IsDBNull(reader.GetOrdinal("FirstName")))
                {
                    rs.FirstName = reader.GetString(reader.GetOrdinal("FirstName"));
                }
                if (!reader.IsDBNull(reader.GetOrdinal("LastName")))
                {
                    rs.LastName = reader.GetString(reader.GetOrdinal("NumberPhone"));
                }
                if (!reader.IsDBNull(reader.GetOrdinal("NumberPhone")))
                {
                    rs.NumberPhone = reader.GetString(reader.GetOrdinal("NumberPhone"));
                }
                if (!reader.IsDBNull(reader.GetOrdinal("Mail")))
                {
                    rs.Mail = reader.GetString(reader.GetOrdinal("Mail"));
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while trying to read the contact data from the database : " + ex.Message);
            }
            return rs;
        }

    }
}
