﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Practice.Contacts.DAL
{
    public class ConnectionString
    {
        public override string ToString()
        {
            return ConfigurationManager.ConnectionStrings["Main"].ConnectionString;
        }
    }
}
