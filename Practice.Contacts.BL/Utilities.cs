﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Practice.Contacts.BL
{
    public class Utilities
    {
        bool invalid = false;
        /// <summary>
        /// Valid if the mail format is correct
        /// </summary>
        /// <param name="strIn"></param>
        /// <returns></returns>
        public bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }

        /// <summary>
        /// the number phone should be of 10 digits
        /// </summary>
        /// <param name="strNumber"></param>
        /// <returns></returns>
        public bool IsValidPhone(string strNumber) { 
            string MatchPhoneNumberPattern = @"^[0-9]{10}$";
            if (strNumber!= null) return Regex.IsMatch(strNumber, MatchPhoneNumberPattern );
            else return false;
        }

        public bool IsValidName(string strName) {
            string MatchNamePattern = @"^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$";
            if (strName != null) return Regex.IsMatch(strName, MatchNamePattern);
            else return false;
        }
    }
}
