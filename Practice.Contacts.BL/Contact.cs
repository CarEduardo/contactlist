﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Practice.Contacts.DAL;
using Practice.Contacts.EL;

namespace Practice.Contacts.BL
{
    public class Contact
    {
        private DAL.Contact contactDAL;
        private Utilities utilities;

        public Contact()
        {
            this.contactDAL = new DAL.Contact();
            this.utilities = new Utilities();
        }

        public EL.Contact GetContact(EL.Contact contact) 
        {
            return contactDAL.GetContact(contact);
        }

        public List<EL.Contact> GetAll()
        {
            return contactDAL.GetAll();
        }

        public EL.Contact Upsert(EL.Contact contact) 
        {
            contact = VerifyDataContact(contact);
            if(contact.IsValid)
            {
                contact= VerifydPhoneNameMail(contact);
                if(contact.IsValid)
                {
                    contact = VerifyMailOrPhone(contact);
                    if(contact.IsValid)
                    {
                        contact = contactDAL.UpsertContact(contact);
                        contact.IsValid = true;
                    }
                }
            }
            return contact;
        }

        public List<EL.Contact> SearchContact(EL.Contact contact) 
        {
            return contactDAL.SearchContact(contact);
        }

        public EL.Contact DeleteContact(EL.Contact contact) 
        {
            return contactDAL.DeleteContact(contact);
        }

        public EL.Contact VerifyDataContact(EL.Contact contac) {
            contac.IsValid = false;
            if (contac.FirstName != "" & contac.FirstName != null)
            {
                if (contac.LastName != "" & contac.LastName != null)
                {
                    if (contac.NumberPhone != "" & contac.NumberPhone != null)
                    {
                        if (contac.Mail != "" & contac.Mail != null)
                        {
                            contac.IsValid = true;
                        } else {
                            contac.Message = "Enter you Email";
                        }
                    }else {
                        contac.Message = "Enter you number phone";
                    }
                }else {
                    contac.Message = "Enter you first name";
                }
            }else{
                contac.Message = "Enter you name";
            }
            return contac;
        }

        public EL.Contact VerifyMailOrPhone(EL.Contact contact) {
            
            EL.Contact contactSearch = contactDAL.SearchPhoneEmail(contact);
            if (contact.ContactId != 0)
            {
                if (contact.ContactId == contactSearch.ContactId)
                {
                    contact.IsValid = true;
                }
                else 
                {
                    contact.IsValid = false;
                    contact.Message = "Another contact already has this number";
                    if(contact.Mail == contactSearch.Mail)
                    {
                        contact.Message = "Another contact already has this email";
                    }
                    
                }
            }
            else
            {
                if (contactSearch.ContactId == 0)
                {
                    contact.IsValid = true;
                }
                else
                {
                    contact.IsValid = false;
                    contact.Message = "Another contact already has this number";
                    if (contact.Mail == contactSearch.Mail)
                    {
                        contact.Message = "Another contact already has this email";
                    }
                }
            }
            return contact;
        }

        public EL.Contact VerifydPhoneNameMail(EL.Contact contact) {
            contact.IsValid = false;
            if (utilities.IsValidEmail(contact.Mail))
            {
                if (utilities.IsValidPhone(contact.NumberPhone))
                {
                    if (utilities.IsValidName(contact.FirstName) & utilities.IsValidName(contact.LastName))
                    {
                        contact.IsValid = true;
                    }else {
                        contact.Message = "The initial of your name must be capital";
                    }
                }else {
                    contact.Message = "Invalid number, must contain 10 digits";
                }
            }else {
                contact.Message = "Invalid email, please add a valid email";
            }
            return contact;
        }

        public EL.Contact Delete(EL.Contact contact)
        {
            return contactDAL.DeleteContact(contact);
        }

        public EL.Contact GetById(EL.Contact contact)
        {
            return contactDAL.GetById(contact);
        }
    }
}
